# GMF2MSH2D
Convert planar 3D GMF mesh in MSH (FreeFem++) 2D format mesh

This is a Python (2 or 3) script to convert a planar GMF (.mesh file) with 3D coordinates in a 2D .msh (FreeFem++ mesh format)

Use: 
$ python gmf2msh.py example.mesh

It creates a file example2d.msh
