#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 30 11:22:02 2018

@author: earanda

script to convert a planar 3D Gamma Format Mesh in 2D .msh format mesh (Freefem++)

use: 
  $ python3 meditmesh2ff+.py file.mesh [output.msh]

"""

import sys
import os

class conversor(object):
    def __init__(self,fichero,salida=None):    
        # check if the file is has .mesh extension
        nombre = os.path.basename(fichero)
        name = os.path.splitext(nombre)
        if name[1] != '.mesh':
            print('It does not seem a GMF file. Aborting...')
            exit()

        self.archivo = fichero
        # Extracción de datos del archivo
        self.tipomalla()
        if salida == None:
            self.salida = name[0] + "2d.msh"
        else:
            self.salida = salida

        
    def tipomalla(self):
        
        with open(self.archivo) as myFile:
            self.fullfile = myFile.readlines()
            
        if self.fullfile == None:
            print("Error reading the file. Aborting...")
            exit()
            
        # check if there is Tetrahedra, therefore this is not a planar mesh 
        # get the key lines
        first = second = third = -2
        for num, line in enumerate(self.fullfile):
            if "Tetrahedra" in line:
                print('This is not a planar mesh. Aborting...')
                exit()
            elif "Vertices" in line:
                first = num
            elif "Triangles" in line:
                second = num
            elif "Edges" in line:
                third = num
                        
        self.n1 = first + 1
        self.n2 = second + 1
        self.n3 = third + 1
        self.p = first + 2
        self.fp = self.n1 + int(self.fullfile[self.n1].strip())
        self.s = second + 2
        self.fs = self.n2 + int(self.fullfile[self.n2].strip())
        self.t = third + 2
        self.ft = self.n3 + int(self.fullfile[self.n3].strip())
        


    def del_z(self,cadena):
        """
        deleting third coordinate
        """
        lis = cadena.strip().split()
        lis.pop(-2)
        return ' '.join(lis) + '\n'
    
    def escribir(self):
        """
        Writing the exit file in .msh format
        """
        with open(self.salida,'w') as f:
        
            # First line of the file: elements or each type
            f.write(self.fullfile[self.n1].strip() + ' ' + self.fullfile[self.n2].strip() + ' ' + self.fullfile[self.n3].strip() + '\n')
        
            # Write file
            for line in self.fullfile[self.p:self.fp+1]:
                f.write(self.del_z(line))
            for line in self.fullfile[self.s:self.fs+1]:
                f.write(line)
            for line in self.fullfile[self.t:self.ft+1]:
                f.write(line)
                   

if __name__== "__main__":
    
    if len(sys.argv) < 2:
        print("Missing mesh file")
        exit()
    elif len(sys.argv) == 3:
        salida = sys.argv[2]
    else:
        salida = None        
    archivo = sys.argv[1]
    
    w = conversor(archivo,salida)
    w.escribir()
    
